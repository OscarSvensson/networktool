package main;

import java.util.Scanner;

import spoofer.ARPSpoofer;


public class NetworkTool {
	
	/*
	 * Useful adress : https://toschprod.wordpress.com/2012/02/21/mitm-6-arp-spoofing-exploit-2/
	 */
	
	public NetworkTool(String[] args) {
		System.out.println(getUserInterface());
		if(args.length == 0) return;
		if(args[0].equals("arpspoof")) {
			if(args.length < 3) {
				System.err.println("To few arguments : Usage NetworkTool arpspoof {targetIP} {gatewayIP}");
				return;
			}
			ARPSpoofer spoofer = new ARPSpoofer(args[1], args[2]);
			
			Runtime.getRuntime().addShutdownHook(getSafeStopHook(spoofer));
			
			Scanner input = new Scanner(System.in);
			String in = "";
			System.out.println("Use s to start spoof and q to stop spoofing");
			do{
				System.out.println("Waiting for command");
				in = input.nextLine();
				if(in.equals("s")) {
					System.out.println("Starts spoofing : " + args[1]);
					spoofer.startSpoof();
				} else if(in.equals("q")) {
					System.out.println("Stops spoofing : " + args[1]);
					spoofer.stopSpoof();
				}
			} while(!in.equals("q"));
			System.out.println("Finished");
			input.close();
			return;
		}
		if(args[0].equals("help")) {
			System.out.println(getUserInterface());
			return;
		}
	}
	
	private Thread getSafeStopHook(ARPSpoofer spoofer) {
		Thread hook = new Thread(new Runnable() {
			public void run() {
				spoofer.stopSpoof();
			}
		});
		return hook;
	}
	
	private String getUserInterface() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("     NetworkTools\n");
		sb.append("-----------------------\n");
		sb.append("Tools:\n");
		sb.append("1. arpspoof {TargetIP} {GatewayIP}\n");
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		new NetworkTool(args);
	}
}