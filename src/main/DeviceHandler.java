package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

public class DeviceHandler {

	@SuppressWarnings("resource")
	public static PcapIf getDevice() {
		List<PcapIf> alldevs = new ArrayList<PcapIf>();
		StringBuilder errBuf = new StringBuilder();
		Scanner input = new Scanner(System.in);
		
		PcapIf dev = null;
		
		System.out.println(getUserInterFace());
		String choice = "", result = "";
		do {
			choice = input.nextLine();
			if(choice.replaceAll("\\s+", "").equals("find")) {
				alldevs.clear();
				int r = Pcap.findAllDevs(alldevs, errBuf);
				if(r == Pcap.NOT_OK || alldevs.isEmpty()) {
					System.out.println(String.format("Can't read devices, error is %s", errBuf.toString()));
					if(errBuf.toString().equals(""))
						System.out.println("Have you forgotten to run as admin?");
					System.exit(1);
				}
				
				System.out.println("Network devices found:");  
				  
		        int i = 0;  
		        for (PcapIf device : alldevs) {  
		            String description =  
		                (device.getDescription() != null) ? device.getDescription()  
		                    : "No description available";  
		            System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);  
		        }
		        System.out.println();
		        result = choice;
			} else if(choice.matches("use \\d")) {
				String[] ch = choice.split(" ");
				result = ch[0];
				System.out.println(result);
				int d = Integer.parseInt(ch[1]);
				
				if(d >= 0 && d < alldevs.size())
					dev = alldevs.get(d);
				else
					System.out.println("No such device...");
			} else if(choice.matches("h")) {
				System.out.println(getUserInterFace());
				result = "";
			} else if(choice.matches("q")) {
				result = choice;
			} else {
				System.out.println("Undefined command...");
			}
			
		} while(!result.equals("q") && !result.equals("use"));
		System.out.println("Finished handling the devices");
		
		//input.close();
		
		return dev;
	}
	
	private static String getUserInterFace() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("     Find Device \n");
		sb.append("-------------------------\n");
		sb.append("Commands: \n");
		sb.append("find\n");
		sb.append("use {index}\n");
		
		return sb.toString();
	}
}
