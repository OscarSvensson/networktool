package main;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Arp;

public class Connector {

	public static void main(String[] args) {  
        List<PcapIf> alldevs = new ArrayList<PcapIf>(); // Will be filled with NICs  
        StringBuilder errbuf = new StringBuilder(); // For any error msgs  
  
        /*************************************************************************** 
         * First get a list of devices on this system 
         **************************************************************************/  
        int r = Pcap.findAllDevs(alldevs, errbuf);
        if(r == Pcap.NOT_OK)
        	System.out.println("Pcap not ok");
        if(alldevs.isEmpty())
        	System.out.println("Alldevs is empty");
        
        if (r == Pcap.NOT_OK || alldevs.isEmpty()) {  
            System.err.printf("Can't read list of devices, error is %s", errbuf  
                .toString());  
            return;  
        }  
  
        System.out.println("Network devices found:");  
  
        int i = 0;  
        for (PcapIf device : alldevs) {  
            String description =  
                (device.getDescription() != null) ? device.getDescription()  
                    : "No description available";  
            System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);  
        }  
  
        Scanner input = new Scanner(System.in);
        System.out.print("Choose device:");
        int d = input.nextInt();
        input.close();
        
        PcapIf device = alldevs.get(d); // We know we have atleast 1 device  
        System.out  
            .printf("\nChoosing '%s' on your behalf:\n",  
                (device.getDescription() != null) ? device.getDescription()  
                    : device.getName());  
  
        /*************************************************************************** 
         * Second we open up the selected device 
         **************************************************************************/  
        int snaplen = 64 * 1024;           // Capture all packets, no trucation  
        int flags = Pcap.MODE_PROMISCUOUS; // capture all packets  
        int timeout = 10 * 1000;           // 10 seconds in millis  
        Pcap pcap =  
            Pcap.openLive(device.getName(), snaplen, flags, timeout, errbuf);  
  
        if (pcap == null) {  
            System.err.printf("Error while opening device for capture: "  
                + errbuf.toString());  
            return;  
        }  
  
        Arp ar = new Arp();
        
        /*************************************************************************** 
         * Third we create a packet handler which will receive packets from the 
         * libpcap loop. 
         **************************************************************************/  
        PcapPacketHandler<String> jpacketHandler = new PcapPacketHandler<String>() {  
  
            public void nextPacket(PcapPacket packet, String user) {  
            	if(packet.hasHeader(ar)) {
            		Arp a = packet.getHeader(new Arp());
            		
            		System.out.println("Got ARP");
            		System.out.println("Name " + a.getName());
            		byte[] shabytes = a.sha();
            		byte[] spabytes = a.spa();
            		byte[] thabytes = a.tha();
            		byte[] tpabytes = a.tpa();
            		
            		int opcode = a.operation();
            		String opDesc = a.operationDescription();
            		
            		String sha, spa, tha, tpa;
            		
					try {
						sha = getHardwareAddress(shabytes);
						spa = InetAddress.getByAddress(spabytes).toString();
						tha = getHardwareAddress(thabytes);
						tpa = InetAddress.getByAddress(tpabytes).toString();
						System.out.println("Sender hardware address - " + sha);
						System.out.println("Sender protocol address - " + spa);
						System.out.println("Target hardware address - " + tha);
						System.out.println("Target protocol address - " + tpa);
						System.out.println("Operation code - " + opcode + " - Desc: " + opDesc);
					} catch (UnknownHostException e) {
						System.out.println("Failed to resolve SPA");
						e.printStackTrace();
					}
					if(opcode == 1) {
						//Forge a new request
					}
            	} //else System.out.println("Got something else");
                /*System.out.printf("Received packet at %s caplen=%-4d len=%-4d %s\n",  
                    new Date(packet.getCaptureHeader().timestampInMillis()),   
                    packet.getCaptureHeader().caplen(),  // Length actually captured  
                    packet.getCaptureHeader().wirelen(), // Original length   
                    user                                 // User supplied object  
                    );  */
            	
            }  
        };  
  
        /*************************************************************************** 
         * Fourth we enter the loop and tell it to capture 10 packets. The loop 
         * method does a mapping of pcap.datalink() DLT value to JProtocol ID, which 
         * is needed by JScanner. The scanner scans the packet buffer and decodes 
         * the headers. The mapping is done automatically, although a variation on 
         * the loop method exists that allows the programmer to sepecify exactly 
         * which protocol ID to use as the data link type for this pcap interface. 
         **************************************************************************/  
        pcap.loop(100, jpacketHandler, "jNetPcap rocks!");  
  
        /*************************************************************************** 
         * Last thing to do is close the pcap handle 
         **************************************************************************/  
        pcap.close();  
    }  
	
	private static String getHardwareAddress(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bytes.length;i++)
			sb.append(String.format("%02X%s", bytes[i], (i < bytes.length - 1) ? "-" : ""));
		return sb.toString();
	}
}
