# README #

### What is this repository for? ###

  NetworkTool is a set of tools which enables spoofing and find live hosts on a network on Windows. It is a testprogram which is only for educational purposes.

### How do I get set up? ###

  Setup : 
  Download the program as a zip. Unzip it in a preferable folder. Make sure to have winpcap(Windows) installed. Also make sure to have JNetpcap downloaded. 
  Then compile it with javac.
  
  Dependencies : 
  Jnetpcap, Winpcap

### Who do I talk to? ###

  OscarSvensson or you can send me an email on wgcp92@gmail.com